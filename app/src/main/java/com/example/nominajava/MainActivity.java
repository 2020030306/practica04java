package com.example.nominajava;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
public class MainActivity extends AppCompatActivity {

    private TextView lblNombreTrabajador;

    private EditText txtNombreTrabajador;


    private Button btnEntrar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        lblNombreTrabajador = (TextView) findViewById(R.id.lblNombreTrabajador);
        txtNombreTrabajador = (EditText) findViewById(R.id.txtNombreTrabajador);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
    }

    private void entrar() {
        String strNombre;

        strNombre = getApplicationContext().getResources().getString(R.string.nombre);

        if (strNombre.toString().equals(txtNombreTrabajador.getText().toString().trim())) {
            // Hacer el paquete para enviar información
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombreTrabajador.getText().toString());

            // Crear el intent para llamar a otra actividad
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad esperando o no respuesta
            startActivity(intent);
        } else {
            Toast.makeText(this.getApplicationContext(), "El usuario o contraseña no es válido", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        android.app.AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Salir de la app?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hace nada
            }
        });
        confirmar.show();
    }
}